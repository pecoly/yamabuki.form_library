﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;

using Yamabuki.Design.Component.Base;
using Yamabuki.Design.Constant;
using Yamabuki.EditorDev;
using Yamabuki.EditorDev.Command;
using Yamabuki.EditorDev.Manager;
using Yamabuki.Utility.Log;
using Yamabuki.Window.ChildForm;
using Yamabuki.Window.ParentForm;

namespace Yamabuki.Form.Library
{
    public class LibraryForm_P
        : ChildForm_P<LibraryForm_VI>
    {
        /// <summary>ロガー</summary>
        private static readonly Logger Logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        private Boolean isInitialized;

        public LibraryForm_P(ParentForm_PI parentForm_P)
            : base(parentForm_P)
        {
        }

        public override event Action<Boolean> Form_VisibleChanged
        {
            add { this.View.Form_VisibleChanged += value; }
            remove { this.View.Form_VisibleChanged -= value; }
        }

        private PluginManager PluginManager { get; set; }

        private ComponentManager ComponentManager { get; set; }

        public static ChildForm_PI Create(ParentForm_PI parentForm_PI, DevContext context)
        {
            var presenter = new LibraryForm_P(parentForm_PI);
            var view = new LibraryForm_V();
            presenter.PluginManager = context.PluginManager;
            presenter.ComponentManager = context.ComponentManager;
            presenter.View = view;
            return presenter;
        }
                
        public void UpdateComponentTree()
        {
            var comTree = new Dictionary<String, List<Tuple<String, String>>>();

            foreach (var comInfo in this.PluginManager.ComponentInfoList.Values)
            {
                var fullName = comInfo.FullName;
                var pathList = fullName.Split(new Char[] { DsComponentConstants.Separator });

                if (pathList.Length != 2)
                {
                    continue;
                }

                if (!comTree.ContainsKey(pathList[0]))
                {
                    var nodeList = new List<Tuple<String, String>>();
                    nodeList.Add(new Tuple<String, String>(pathList[1], comInfo.ClassName));
                    comTree.Add(pathList[0], nodeList);
                }
                else
                {
                    comTree[pathList[0]].Add(new Tuple<String, String>(pathList[1], comInfo.ClassName));
                }
            }

            foreach (var kv in comTree)
            {
                foreach (Tuple<String, String> value in kv.Value)
                {
                    this.View.AddNode(kv.Key, value.Item1, value.Item2);
                }
            }
        }

        protected override void OnViewSet()
        {
            if (this.isInitialized)
            {
                Debug.Assert(false);
                return;
            }

            this.isInitialized = true;

            base.OnViewSet();

            this.View.Form_Load = this.Form_Load;
            this.View.CreateComponentCreationCommand = this.CreateComponentCreationCommand;
        }

        private void Form_Load()
        {
            try
            {
                Logger.BeginMethod(MethodBase.GetCurrentMethod());
                this.View.Initialize();
                this.UpdateComponentTree();
            }
            finally
            {
                Logger.EndMethod(MethodBase.GetCurrentMethod());
            }
        }

        private ComponentCreationCommand CreateComponentCreationCommand(String className)
        {
            var createComponentFunc = new Func<Point, String, DsComponentImpl, DsComponentImpl>((point, newName, parent) =>
            {
                return this.ComponentManager.CreateComponent(
                    className, point.X, point.Y, newName, parent);
            });

            var getNameFunc = new Func<String>(() =>
            {
                return this.ComponentManager.GetComponentName(className);
            });

            var canCreateFunc = new Func<DsComponentImpl, DsComponentImpl, String>((parent, current) =>
            {
                return current.CanCreate(parent);
            });

            return new ComponentCreationCommand(createComponentFunc, getNameFunc, canCreateFunc);
        }
    }
}
