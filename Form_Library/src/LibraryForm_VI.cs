﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Yamabuki.EditorDev.Command;
using Yamabuki.Window.ChildForm;

namespace Yamabuki.Form.Library
{
    public interface LibraryForm_VI
        : ChildForm_VI
    {
        event Action<Boolean> Form_VisibleChanged;

        Action Form_Load { set; }

        Func<String, ComponentCreationCommand> CreateComponentCreationCommand { get; set; }

        void AddNode(String parentNodeText, String childNodeText, String className);
    }
}
