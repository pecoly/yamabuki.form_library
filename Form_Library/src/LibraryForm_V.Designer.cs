﻿namespace Yamabuki.Form.Library
{
    partial class LibraryForm_V
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.libraryTreeView = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // libraryTreeView
            // 
            this.libraryTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.libraryTreeView.Location = new System.Drawing.Point(0, 0);
            this.libraryTreeView.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.libraryTreeView.Name = "libraryTreeView";
            this.libraryTreeView.Size = new System.Drawing.Size(454, 324);
            this.libraryTreeView.TabIndex = 0;
            // 
            // LibraryForm_V
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 324);
            this.Controls.Add(this.libraryTreeView);
            this.DockAreas = ((WeifenLuo.WinFormsUI.Docking.DockAreas)(((((WeifenLuo.WinFormsUI.Docking.DockAreas.DockLeft | WeifenLuo.WinFormsUI.Docking.DockAreas.DockRight) 
            | WeifenLuo.WinFormsUI.Docking.DockAreas.DockTop) 
            | WeifenLuo.WinFormsUI.Docking.DockAreas.DockBottom) 
            | WeifenLuo.WinFormsUI.Docking.DockAreas.Document)));
            this.Font = new System.Drawing.Font("メイリオ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "LibraryForm_V";
            this.Text = "コンポーネントライブラリ";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView libraryTreeView;
    }
}