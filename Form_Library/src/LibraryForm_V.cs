﻿using System;
using System.Drawing;
using System.Windows.Forms;

using WeifenLuo.WinFormsUI.Docking;

using Yamabuki.EditorDev.Command;
using Yamabuki.Window.ChildForm;
using Yamabuki.Window.Utility;

namespace Yamabuki.Form.Library
{
    internal partial class LibraryForm_V
        : ChildForm_V, LibraryForm_VI
    {
        private EventHandler form_Load;

        public LibraryForm_V()
        {
            this.InitializeComponent();
        }

        public Action Form_Load
        {
            set
            {
                this.Load -= this.form_Load;
                this.form_Load = (sender, e) => value();
                this.Load += this.form_Load;
            }
        }

        public override String PersistString
        {
            get { return "Yamabuki.Form.Library.LibraryForm"; }
        }

        public override DockState DefaultState
        {
            get { return DockState.DockRight; }
        }

        public Func<String, ComponentCreationCommand> CreateComponentCreationCommand { get; set; }

        public override void Initialize()
        {
            this.libraryTreeView.BeforeLabelEdit += this.TreeView_BeforeLabelEdit;
            this.libraryTreeView.MouseDown += this.TreeView_MouseDown;
            this.libraryTreeView.DragOver += this.TreeView_DragOver;
            this.libraryTreeView.AfterSelect += this.TreeView_AfterSelect;
        }
        
        public void AddNode(String parentNodeText, String childNodeText, String className)
        {
            var parentNode = TreeNodeCollectionUtils.FindTreeNodeByText(this.libraryTreeView.Nodes, parentNodeText);

            var childNode = new TreeNode(childNodeText);
            childNode.Tag = className;

            if (parentNode == null)
            {
                parentNode = new TreeNode(parentNodeText);
                this.libraryTreeView.Nodes.Add(parentNode);
            }

            parentNode.Nodes.Add(childNode);
        }

        private void TreeView_AfterSelect(Object sender, TreeViewEventArgs e)
        {
            this.libraryTreeView.Focus();
        }

        private void TreeView_BeforeLabelEdit(Object sender, NodeLabelEditEventArgs e)
        {
            this.libraryTreeView.SelectedNode = e.Node;
        }

        private void TreeView_MouseDown(Object sender, MouseEventArgs e)
        {
            var node = this.libraryTreeView.GetNodeAt(new Point(e.X, e.Y));
            if (e.Button == MouseButtons.Right)
            {
                this.libraryTreeView.SelectedNode = node;
            }

            if (node == null)
            {
                return;
            }

            var type = node.Tag as String;
            if (String.IsNullOrEmpty(type))
            {
                return;
            }

            this.libraryTreeView.SelectedNode = node;
            this.libraryTreeView.DoDragDrop(
                this.CreateComponentCreationCommand(type), DragDropEffects.Move);
        }

        private void TreeView_DragOver(Object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }
    }
}
